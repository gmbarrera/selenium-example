import time, unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


SLEEP_TIME = 2


class TestCRUDSampleModel(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        # self.driver = webdriver.Chrome()
        

    def test_crud(self):
        self.driver.get("http://localhost:8000/admin")
        
        self.assertIn("Django", self.driver.title)

        time.sleep(SLEEP_TIME)

        elem = self.driver.find_element_by_name("username")
        elem.clear()
        elem.send_keys("gabriel")

        time.sleep(SLEEP_TIME)

        elem = self.driver.find_element_by_name("password")
        elem.clear()
        elem.send_keys("1234")

        time.sleep(SLEEP_TIME)

        elem.send_keys(Keys.RETURN)

        time.sleep(SLEEP_TIME)

        elem = self.driver.find_element_by_xpath(".//a[contains(@href,'samplemodel/add')]")
        elem.click()

        self.assertIn('Add sample model', self.driver.title)

        time.sleep(SLEEP_TIME)

        elem = self.driver.find_element_by_id('id_name')
        elem.clear()
        elem.send_keys("El Zorro")
        elem.send_keys(Keys.TAB)

        time.sleep(SLEEP_TIME)

        elem = self.driver.find_element_by_id('id_number')
        elem.clear()
        elem.send_keys("12")

        time.sleep(SLEEP_TIME)

        elem.send_keys(Keys.RETURN)

        time.sleep(SLEEP_TIME)

        elem = self.driver.find_element_by_name('_selected_action')
        elem.click()

        time.sleep(SLEEP_TIME)

        elem = self.driver.find_element_by_name('action')
        elem.send_keys(Keys.ARROW_DOWN)

        time.sleep(SLEEP_TIME)

        elem = self.driver.find_element_by_name('index')
        elem.click()

        time.sleep(SLEEP_TIME)

        elem = self.driver.find_element_by_xpath(".//input[contains(@value,'Yes')]")
        elem.click()

        time.sleep(SLEEP_TIME * 2)

        self.driver.close()


if __name__ == '__main__':
    unittest.main()